<?php

add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles');
function my_theme_enqueue_styles(){
	wp_enqueue_style('parent', get_template_directory_uri() . '/style.css');
}

function social_short(){
	return "<h2 id='social_message'>Please Like and Follow our Page on Facebook and Instagram!</h2>";
}

function mobile_short(){
	return "<h4 style='text-align: center;'>Mobile Number: 99121212</h4>";
}

function about_image_short($atts, $content = null) {
    extract(shortcode_atts(array("src" => ''), $atts));
    return '<img src="' . $src . '" alt="'. do_shortcode($content) .'" />';
}

function about_text_short(){
	return "<p id='about_text_shortcode' >Our company has been offering the latest gadgets and technologies for the past decade! 
	Our motto has and always will be to put our customers first of all</p>";
}

function facebook_short(){
	return '<a id="facebook_link" href="https://www.facebook.com/">Our Facebook Page</a>';
}

function instagram_short(){
	return '<a id="instagram_link" href="https://www.instagram.com/">Our Instagram Page</a>';
}

function table_short(){
	return '<table id="short_table"><tr><th>Website Names</th><th>Website Links</th>
			<tr><td>Apple</td><td><a href="https://www.apple.com/">apple.com</a></td></tr>
			<tr><td>Samsung</td><td><a href="https://www.samsung.com/us/">samsung.com</a></td></tr></table>';
}

add_shortcode('Social_Short', 'social_short');
add_shortcode('Mobile_Contact', 'mobile_short');
add_shortcode('About_Us_Image', 'about_image_short');
add_shortcode('About_Us_Text', 'about_text_short');
add_shortcode('Facebook_Page', 'facebook_short');
add_shortcode('Instagram_Page', 'instagram_short');
add_shortcode('Table_Links', 'table_short');
?>